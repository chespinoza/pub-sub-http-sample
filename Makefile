PROJECT_NAME := "pub-sub-http-sample"
VERSION = 0.0.1
BUILD = $(shell git rev-parse --short=6 HEAD)
LDFLAGS = -v -ldflags "-X main.Version=${VERSION} -X main.Build=${BUILD}"
PKG := "gitlab.com/chespinoza/${PROJECT_NAME}"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
DOCKER_REPO = chespinoza/${PROJECT_NAME}
BUILD_DIR = build

.PHONY: dep lint build clean test docker

default: help

dep: ## Get the dependencies for CI
	@go get -v -d ./...
	@go get -u github.com/golang/lint/golint

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

build: ## Build binaries for current host
	mkdir -p $(BUILD_DIR)
	@go build $(LDFLAGS) -o $(BUILD_DIR)/server ./cmd/server

clean: ## Clean files generated on build
	rm -rf build/*

docker: ## Build binaries into docker container
	docker build --force-rm -t $(DOCKER_REPO):$(VERSION) --build-arg build=$(BUILD) --build-arg version=$(VERSION) .

help: ## Help
	@echo "Please use 'make <target>' where <target> is ..."
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
