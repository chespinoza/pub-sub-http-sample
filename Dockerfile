FROM golang:alpine AS builder
RUN apk --no-cache add \
    build-base \
    bash \
    git
ADD . /go/src/gitlab.com/chespinoza/pub-sub-http-sample
WORKDIR /go/src/gitlab.com/chespinoza/pub-sub-http-sample
RUN echo 'Building binary with: ' && go version
RUN make build

FROM alpine
RUN apk --no-cache add \
    ca-certificates
WORKDIR /app
EXPOSE 8080
COPY --from=builder /go/src/gitlab.com/chespinoza/pub-sub-http-sample/build/server /app/
CMD /app/server
