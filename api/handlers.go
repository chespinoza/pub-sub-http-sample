package api

import (
	"encoding/json"
	"log"
	"net/http"
)

// subscribeHandler subscribe a user in the pubsub system
// is required to do so in order to own a message box
// Method POST,/api/v1/subscribe, json payload {"user_name":"chris"}
func subscribeHandler(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		UserName string `json:"user_name"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		log.Println(r.Body)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	subscribersNum, err := pubSubBroker.Subscribe(payload.UserName)
	if err != nil {
		log.Println("error subscribing user:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, struct {
		Result      string `json:"response"`
		Subscribers int    `json:"num_subscribers"`
	}{"ok", subscribersNum})
}

// unsubscribeHandler unsubscribe a user from pubsub system
//  Method DELETE, /api/v1/subscribe, json payload {"user_name":"chris"}
func unsubscribeHandler(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		UserName string `json:"user_name"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		log.Println(r.Body)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err := pubSubBroker.Unsubscribe(payload.UserName)
	if err != nil {
		log.Println("error trying to unsubscribe user:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, struct {
		Result string `json:"response"`
	}{"ok"})
}

// publishHandler publish a message to all the user subscribed
//  Method POST, /api/v1, json payload {"message":"hello"}
func publishHandler(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		Message string `json:"message"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		log.Println(r.Body)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	pubSubBroker.Publish(payload.Message)

	writeJSON(w, struct {
		Result string `json:"response"`
	}{"ok"})
}

// retrieveHandler retrieves a message from pubsubsystem message box
//  Method GET, /api/v1, json payload {"user_name":"chris"}
func retrieveHandler(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		UserName string `json:"user_name"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		log.Println(r.Body)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	msg, err := pubSubBroker.Retrieve(payload.UserName)
	if err != nil {
		log.Println("error retrieving messages:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, struct {
		Message string `json:"message"`
	}{msg})
}
