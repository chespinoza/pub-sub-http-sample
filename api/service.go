package api

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/chespinoza/pub-sub-http-sample/pubsub"
)

// Service abstraction
type Service struct {
	router *chi.Mux
	addr   string
}

var pubSubBroker pubsub.Broker

// Start init api service
func (s *Service) Start() error {

	pubSubBroker = pubsub.NewBroker()

	s.router = chi.NewRouter()
	s.router.Post("/api/v1/subscribe", subscribeHandler)
	s.router.Delete("/api/v1/susbcribe", unsubscribeHandler)
	s.router.Post("/api/v1", publishHandler)
	s.router.Get("/api/v1", retrieveHandler)
	return http.ListenAndServe(s.addr, s.router)
}

// New - Service contructor
func New(addr string) *Service {
	if len(addr) == 0 {
		log.Fatal("addr for http service is required", addr)
	}
	service := &Service{
		addr: addr,
	}
	return service
}
