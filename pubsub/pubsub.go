package pubsub

import (
	"errors"
	"log"
)

// MessageBox Simple message box for messages
type MessageBox chan string

// Broker is a simple broker without topics
type Broker map[string]MessageBox

// NewBroker Broker constructor
func NewBroker() Broker {
	return make(Broker)
}

// Subscribe add a new subscriber
// to be used to request published messages in the system.
// the method returns the number of current subscribers and
// an error if the user string is blank
func (b Broker) Subscribe(user string) (int, error) {
	if len(user) == 0 {
		return 0, errors.New("user->length is zero")
	}

	if _, ok := b[user]; !ok {
		b[user] = make(chan string)
	}
	return len(b), nil
}

// Unsubscribe - Removes a subcription for a given user
// returns error if the user doesn't exist
func (b Broker) Unsubscribe(user string) error {
	// Exists the user?
	if _, ok := b[user]; ok {
		if _, ok := b[user]; ok {
			delete(b, user)
			return nil
		}
	}
	return errors.New("The subscription does not exist")
}

// Publish - Publish a message for every user that has a subscrition
// the messages are sended to the user messagebox
func (b Broker) Publish(msg string) {
	go func() {
		for _, msgBox := range b {
			msgBox <- msg
		}
	}()
}

// Retrieve - get messages for a given user
// - Retrieval succeeded.
// - The subscription does not exist
// - There are no messages available for this user.
func (b Broker) Retrieve(user string) (string, error) {
	select {
	case msg, ok := <-b[user]:
		if ok {
			return msg, nil
		}
		return "", errors.New("not found")
	default:
		// no messages
		log.Printf("Debug:%v", b[user])
		return "", nil
	}
}
