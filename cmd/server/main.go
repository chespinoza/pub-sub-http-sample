package main

import (
	"log"

	"gitlab.com/chespinoza/pub-sub-http-sample/api"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	api := api.New(":8080")
	if err := api.Start(); err != nil {
		log.Fatal(err)
	}
}
